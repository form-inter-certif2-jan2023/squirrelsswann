
<!-- README.md is generated from README.Rmd. Please edit that file -->

# squirrelsswann <a><img src='./man/figures/squirrels_hex.png' align="right" height="160" /></a>

<!-- badges: start -->
<!-- badges: end -->

The goal of squirrelsswann is to study the population of squirrels in
Central Park.

## Installation

You can install the development version of squirrelsswann like so:

``` r
remotes::install_local(path = "~/squirrelsswann_0.0.0.9000.tar.gz", build_vignettes = TRUE)
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(squirrelsswann)
## basic example code
get_message_fur_color(primary_fur_color = "Gray")
#> We will focus on Gray squirrels
```
